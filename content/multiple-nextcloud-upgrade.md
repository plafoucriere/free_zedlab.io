Title: Mises à jour multiples de Nextcloud
Date: 2020-03-28 22:35
Category: Bloc-notes
Status: published
Summary: De Nextcloud 12 à 18, de Debian Stretch à Buster
Tags: nextcloud, admin, debian, cli, shell, web, privacy, backup, mariadb

**Sur une installation _standard_ basée sur :**

- Debian/GNU Linux
- Apache HTTPD
- MariaDB

**Mises à jours concernées :**

- de _Nextcloud_ 12 à 18
- de _Debian/Stretch_ à _Debian/Buster_

**Remarques :**

Pour les étapes de [mise à jour manuelle][1], j'ai fais un script _basique_ («[`update script`][5]» ci dessous) intégrant le mode _maintenance_ et la sauvegarde.

Les choix des versions suivantes de _Nextcloud_ ont été celles suggérée par l'admin _Nextcloud_.

La mise à jour _Stretch_ > _Buster_ c'est faite le plus tard possible : lors de la fin de support de `php7.0` pour _Nextcloud_ v16.0.8

Les messages d'erreurs ont été laissés tel quels, et donc en Français pour ceux qui ont été traduits.

---

Check-list suivie :
-----------------

_(Les emoji ajoutent du context, détails sur [gitmoji][3] )_

1. ☑ 📢 Prevent users by mail
1. ☑ 🚧 Activate maintenance mode
1. ☑ ⬆️ Upgrade Debian/Stretch
1. ☑ 🚧 Deactivate maintenance mode
1. ☑ Reboot
1. ☑ ⬆️ Upgrade Nextcloud : v12.0.4.3 > v13.0.12 ([docs][4])
    -   «[`update script`][5]»
1. ☑ web/DAV/desktop/android tests
1. ☑ ⬆️ Upgrade Nextcloud : v13.0.12 > v14.0.14 - ([docs][6])
    -   «[`update script`][5]»
1. ☑ web/DAV/desktop/[logging][8] tests
    -   🚑 Error PHP : `You are using a fallback implementation of the intl extension. Installing the native one is highly recommended instead. at /srv/freezed.me/nextcloud/3rdparty/patchwork/utf8/src/Patchwork/Utf8/Bootup/intl.php#18`
        -   ➕ `apt install php-intl`
        -   ➕ `phpenmod intl`
    -    Set cron to AJAX
    -    Set SMTP config
    -    Set cron to local `crontab`
    -   🚑 `La configuration du serveur web ne permet pas d'atteindre "/.well-known/caldav"`
    -   🚑 `La configuration du serveur web ne permet pas d'atteindre "/.well-known/carddav"`
        -   `a2enmod rewrite`
        -   [docs][9]
    -   🚑 `Info cli Memcache \OC\Memcache\APCu not available for distributed|local cache`
        -   🔧 set the `apc.enable_cli = 1` in `/etc/php/7.0/cli/php.ini` (just before last line, see [docs][10])
    -   ⚡ _indice warnings_, see [migration from Nextcloud 13][11] in docs.
        -   🔧 `sudo -u www-data php occ db:add-missing-indices`
    -   ⚡ see [migration from Nextcloud 13][11] in docs.
        -   🔧 `sudo -u www-data php occ db:convert-filecache-bigint`
1. ☑ upgrade Nextcloud : v14.0.14 > v15.0.11 - ([docs][13])
    -   «[`update script`][5]»
1. ☑ web/DAV/desktop/android tests
1. ☑ web admin [overview/logging][15]
    -   🚑 \`La base de données a quelques index manquant. L'ajout d'index dans de grandes tables peut prendre un certain temps. Elles ne sont donc pas ajoutées automatiquement. En exécutant "occ db:add-missing-indices", ces index manquants pourront être ajoutés manuellement pendant que l'instance continue de tourner. Une fois les index ajoutés, les requêtes sur ces tables sont généralement beaucoup plus rapides.
        -   `Index "owner_index" manquant dans la table "oc_share".`
        -   `Index "initiator_index" manquant dans la table "oc_share".`
            -   🔧 `occ db:add-missing-indices`
    -   ⚠️ `Error PHP Undefined index: changelogURL at /srv/freezed.me/nextcloud/core/Controller/WhatsNewController.php#91`
        -   See [#19490][16], will be solved in [v16][17]
    -   ⚠️ `MySQL est utilisée comme base de données mais ne supporte pas les caractères codés sur 4 octets. Pour pouvoir manipuler les caractères sur 4 octets (comme les émoticônes) sans problème dans les noms de fichiers ou les commentaires par exemple, il est recommandé d'activer le support 4 octets dans MySQL.` [Pour plus de détails, lisez la page de documentation à ce sujet][2]
        -   Wait after step «`Upgrade Nextcloud : v17.0.5 > v18.0.3`»
    -   ⚠️ `Vous utilisez actuellement PHP 7.0.33-0+deb9u7. Mettez à jour votre version de PHP afin de tirer avantage des améliorations liées à la performance et la sécurité fournies par le PHP Group dès que votre distribution le supportera.`
        -   Wait after `dist-upgrade Debian/Stretch > Buster`
1. ☑ ⬆️ Upgrade Nextcloud : v15.0.11 > v15.0.14 - ([docs][18])
    -   «[`update script`][5]»
1. ☑ web/DAV/desktop/android tests
1. ☑ Web/DAV/desktop/[settings][15]
    -   ⚠️ `MySQL est utilisée comme base de données mais ne supporte pas les caractères codés sur 4 octets. Pour pouvoir manipuler les caractères sur 4 octets (comme les émoticônes) sans problème dans les noms de fichiers ou les commentaires par exemple, il est recommandé d'activer le support 4 octets dans MySQL.` [Pour plus de détails, lisez la page de documentation à ce sujet][2]
        -   see up
    -   ⚠️ `Vous utilisez actuellement PHP 7.0.33-0+deb9u7. Mettez à jour votre version de PHP afin de tirer avantage des améliorations liées à la performance et la sécurité fournies par le PHP Group dès que votre distribution le supportera.`
        -   see up
    -   ⚡ `Certaines colonnes de la base de données n'ont pas été converties en big int. Changer le type de colonne dans de grandes tables peu prendre beaucoup de temps, elles n'ont donc pas été converties automatiquement. En exécutant 'occ db:convert-filecache-bigint' ces changements en suspens peuvent être déclenchés manuellement. Cette opération doit être exécutée pendant que l'instance est hors ligne. Pour plus d'information, consulter la page de la documentation.`
        -   `mounts.storage_id`
        -   `mounts.root_id`
        -   `mounts.mount_id`
            -   🔧 `sudo -u www-data php occ db:add-missing-indices`
1. ☑ ⬆️ Upgrade Nextcloud : v15.0.14 > v16.0.8 - ([docs][21])
    -   «[`update script`][5]»
        -   💥 `This version of Nextcloud requires at least PHP 7.1<br/>You are currently running 7.0.33-0+deb9u7. Please update your PHP version.zsh: exit 255 sudo -u www-data php occ upgrade`
    -   ⏪ stash 16.0.9 : `mv nextcloud next-nextcloud-16.0.8`
    -   ⏪ rollback to 15.0.14 : `mv old-nextcloud-15.0.14 nextcloud`
1. ☑ ⬆️ dist-upgrade Debian/Stretch > Buster
    -   `apt autoremove`
    -   📸 update again to 16.0.9 : `mv nextcloud next-nextcloud-16.0.8`
    -   📸 backup 15.0.14 : `mv old-nextcloud-15.0.14 nextcloud`
    -   ⬆️ `apt install php7.3-mysql` ([see][23])
    -   ⬆️ `a2enmod php7.3`
1. ☑ ⬆️ Upgrade Nextcloud : v16.0.8 > v16.0.9
    -   «[`update script`][5]»
1. ☑ ⬆️ Upgrade Nextcloud : v16.0.9 > v17.0.5
    -   «[`update script`][5]»
1. ☑ web/DAV/desktop/android tests
1. ☑ web admin [overview/logging][15]
    -   🚑 `Info cli Memcache \OC\Memcache\APCu not available for distributed|local cache`
        -   🔧 set `apc.enable_cli = 1` in `/etc/php/7.3/cli/php.ini` (just before last line) ([docs][27])
    -   ⚡ `La base de données a quelques index manquant. L'ajout d'index dans de grandes tables peut prendre un certain temps. Elles ne sont donc pas ajoutées automatiquement. En exécutant "occ db:add-missing-indices", ces index manquants pourront être ajoutés manuellement pendant que l'instance continue de tourner. Une fois les index ajoutés, les requêtes sur ces tables sont généralement beaucoup plus rapides.`
        -   `Index "twofactor_providers_uid" manquant dans la table "oc_twofactor_providers".`
        -   `Index "version" manquant dans la table "oc_whats_new".`
        -   `Index "cards_abid" manquant dans la table "oc_cards".`
        -   `Index "cards_prop_abid" manquant dans la table "oc_cards_properties".`
            -   🔧 `sudo -u www-data php occ db:add-missing-indices`
    -   ⚠️ `La limite de mémoire PHP est inférieure à la valeur recommandée de 512 Mo.`
        -   Wait after step «`Upgrade Nextcloud : v17.0.5 > v18.0.3`»
    -   ⚠️ `MySQL est utilisée comme base de données mais ne supporte pas les caractères codés sur 4 octets. Pour pouvoir manipuler les caractères sur 4 octets (comme les émoticônes) sans problème dans les noms de fichiers ou les commentaires par exemple, il est recommandé d'activer le support 4 octets dans MySQL.` [Pour plus de détails, lisez la page de documentation à ce sujet][2]
        -   Wait after step «`Upgrade Nextcloud : v17.0.5 > v18.0.3`»
1.  ☑ ⬆️ Upgrade Nextcloud : v17.0.5 > v18.0.3
    -   «[`update script`][5]»
1.  ☑ web/DAV/desktop/android tests
1.  ☑ web admin [overview/logging][15]
    -   🚑 `La limite de mémoire PHP est inférieure à la valeur recommandée de 512 Mo.`
        - 🔧 set `memory_limit = 512M` in `/etc/php/7.3/apache2/php.ini`
    -   ⚡ `La base de données a quelques index manquant. L'ajout d'index dans de grandes tables peut prendre un certain temps. Elles ne sont donc pas ajoutées automatiquement. En exécutant "occ db:add-missing-indices", ces index manquants pourront être ajoutés manuellement pendant que l'instance continue de tourner. Une fois les index ajoutés, les requêtes sur ces tables sont généralement beaucoup plus rapides.`
        -   `Index "calendarobject_calid_index" manquant dans la table "oc_calendarobjects_props".`
        -   `Index "schedulobj_principuri_index" manquant dans la table "oc_schedulingobjects".`
        -   🔧 `sudo -u www-data php occ db:add-missing-indices`
    -   ⚠️ `MySQL est utilisée comme base de données mais ne supporte pas les caractères codés sur 4 octets. Pour pouvoir manipuler les caractères sur 4 octets (comme les émoticônes) sans problème dans les noms de fichiers ou les commentaires par exemple, il est recommandé d'activer le support 4 octets dans MySQL.` [Pour plus de détails, lisez la page de documentation à ce sujet][2]
        -   🔧 Enabling _MySQL 4-byte support_ : [see docs][2]


[1]:    https://docs.nextcloud.com/server/stable/admin_manual/maintenance/manual_upgrade.html#upgrade-manually
[2]:    https://docs.nextcloud.com/server/18/admin_manual/configuration_database/mysql_4byte_support.html#mariadb-10-3-or-later
[3]:    https://gitmoji.carloscuesta.me/
[4]:    https://docs.nextcloud.com/server/13/admin_manual/installation/system_requirements.html#server
[5]:    https://gitlab.com/free_zed/shell/-/blob/5dad4c1158a2caf932c5a23e7bcfbb61afa0d220/nextcloud/manual-upgrade.sh
[6]:    https://docs.nextcloud.com/server/14/admin_manual/installation/system_requirements.html#server
[8]:    https://example.com/index.php/settings/admin/logging
[9]:    https://docs.nextcloud.com/server/14/admin_manual/issues/general_troubleshooting.html#service-discovery
[10]:   https://docs.nextcloud.com/server/14/admin_manual/configuration_server/caching_configuration.html#id1
[11]:   https://docs.nextcloud.com/server/14/admin_manual/maintenance/upgrade.html?highlight=missing%20indice#upgrading-to-nextcloud-13
[13]:   https://docs.nextcloud.com/server/15/admin_manual/installation/system_requirements.html#server
[15]:   https://example.com/index.php/settings/admin/overview
[16]:   https://github.com/nextcloud/server/issues/19490
[17]:   https://github.com/nextcloud/server/pull/19465#issuecomment-586165298
[18]:   https://docs.nextcloud.com/server/15/admin_manual/installation/system_requirements.html#server
[21]:   https://docs.nextcloud.com/server/16/admin_manual/installation/system_requirements.html#server
[23]:   https://help.nextcloud.com/t/uncaught-doctrine-dbal-dbalexception-failed-to-connect-to-the-database/8850/2
[27]:   https://docs.nextcloud.com/server/16/admin_manual/configuration_server/caching_configuration.html#id1
