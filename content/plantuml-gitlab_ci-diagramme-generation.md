Title: Génération de diagrammes PlantUML avec Gitlab-CI
Date: 2019-07-13 10:00
Summary: Générer des diagrammes PlantUML à l'aide de l'intégration continue & l'hébergement statique de Gitlab
Category: Bloc-notes
Tags: uml, gitlab, gitlab-pages, statique, ci, dry, devops, web, digitalisation
Status: published
Lang: fr
Slug: plantuml-gitlab_ci-diagramme-generation


_Le moyen le plus simple de garder les diagrammes à jour avec le code_

C'est quoi ?
------

Les diagrammes sont :

* décrits **100%** par du texte
* générés avec _gitlab-ci_
* déployés sur _gitlab-pages_

Tellement facile à maintenir que vos diagrammes _pourraient_ être encore à jour même **10 mois après le démarrage du projet**!!!

Mais comment ?
-----

Poussez simplement [les sources des diagrammes][src] et [.gitlab-ci.yml][yml] se chargera de la génération des images & de la mise en ligne via _pages_ :

![Modèle physique de donnée avec fichiers partagés][pdm-shared].

[D.R.Y.][dry] !
---------------

L'utilisation de directives du _[préprocesseur PlantUML][preprocessor]_ telles que _!include_ ou _!includeurl_ permet de partager des sources communes entre les diagrammes.

Dans mon [exemple][repo], les diagrammes marqués _shared files_ [partagent les mêmes entitiés][part] :

- associations
- attributs
- classes
- clés
- légende
- relations

Par conséquent, si vous ajoutez quelque chose dans vos _sources partagées_, tous les diagrammes l'obtiendront !

_«[Talk is cheap. Show me the code.][show]»_
------------------------------------------

| _Type de diagramme_          | Fichier seul                                             | Fichiers partagés |
|:---:                         |:---:                                                     |:---:|
|_Domaine fonctionnel_         | -                                                        | ([diagramme][fm-shared]) - [[source][fm-shared-src]] - [éléments communs][part]     |
|_Domaine fonctionnel complet_ | ([diagramme][fmwa-single]) - [[source][fmwa-single-src]] | ([diagramme][fmwa-shared]) - [[source][fmwa-shared-src]] - [éléments communs][part] |
|_Modèle physique de donnée_   | ([diagramme][pdm-single]) - [[source][pdm-single-src]]   | ([diagramme][pdm-shared]) - [[source][pdm-shared-src]] - [éléments communs][part]   |


[dry]: https://en.wikipedia.org/wiki/Don't_repeat_yourself "Don't repeat yourself"
[fm-shared]: https://free_zed.gitlab.io/mypumlt/diagrams/fm-shared_files.png "Domaine fonctionnel avec fichiers partagés"
[fm-shared-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/fm-shared_files.puml "Sources du diagramme"
[fmwa-single]: https://free_zed.gitlab.io/mypumlt/diagrams/fmwa-single_file.png "Domaine fonctionnel (avec attributs) single file"
[fmwa-single-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/fmwa-single_file.puml "Sources du diagramme"
[fmwa-shared]: https://free_zed.gitlab.io/mypumlt/diagrams/fmwa-shared_files.png "Domaine fonctionnel (avec attributs) avec fichiers partagés"
[fmwa-shared-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/fmwa-shared_files.puml "Sources du diagramme"
[part]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/part "Sources partagées"
[pdm-shared]: https://free_zed.gitlab.io/mypumlt/diagrams/pdm-shared_files.png "Modèle physique de donnée avec fichiers partagés"
[pdm-shared-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/pdm-shared_files.puml "Sources du diagramme"
[pdm-single]: https://free_zed.gitlab.io/mypumlt/diagrams/pdm-single_file.png "Modèle physique de donnée single file"
[pdm-single-src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/pdm-single_file.puml "Sources du diagramme"
[plantuml]: http://plantuml.com "Open-source tool to draw UML (or not) diagrams"
[preprocessor]: http://plantuml.com/en/preprocessing#9 "Preprocessing"
[repo]: https://gitlab.com/free_zed/mypumlt
[show]: https://lkml.org/lkml/2000/8/25/132
[src]: https://gitlab.com/free_zed/mypumlt/blob/master/puml/ "Sources du diagramme"
[yml]: https://gitlab.com/free_zed/mypumlt/blob/master/.gitlab-ci.yml
