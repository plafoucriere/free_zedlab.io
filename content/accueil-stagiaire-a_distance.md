Title: Accueil d'un stagiaire à distance
Date: 2020-07-21 21:32
Summary: Essai de traduction de 2 articles traitant de l'accueil de stagiaire à distance et bref retour d'expérience.
Status: published
Category: Bloc-notes
Tags: dev, comportement, innovation, collectif, méthode, agile, gitlab, gitlab-pages

---


Contexte ?
----------

En plein _confinement CoViD-19_, confirmation est faite de l'arrivée d'un nouveau collaborateur pour un stage de 6 mois à mes côtés. La situation est une première pour moi a plusieurs niveau : c'est mon 1er mentorat d'une telle durée dans mon nouveau métier et ça commencera 100% à distance.

De quoi s'agit-il ?
-------------------

Ce billet est une traduction libre, personnelle, synthétisée et certainement imparfaites des pages suivantes :

- 🇬🇧 «[Considerations for remote internships](https://about.gitlab.com/company/culture/all-remote/internship/)» par [_GitLab_](https://about.gitlab.com/company/)
- 🇬🇧 «[Best practices for creating a successful virtual internship](https://www.hbs.edu/recruiting/blog/post/best-practices-for-creating-a-successful-virtual-internship)» par [_Rebecca Carnahan_](https://www.linkedin.com/in/beccacarnahan) (_Harvard Business School Blog_)

J'y ai ajouté quelques notes personnelles en fin de billet.

---


Remarques concernant les stages à distance
------------------------------------------

### Anticipation

- anticiper les méthodes de travail
- s'attendre à ce que les stagiaires aient besoin de beaucoup d'aide
- prévoir une communication claire et asynchrones
- mentor et stagiaire doivent apprécier et respecter l'autonomie
- formation du mentor (centrée sur l'empathie)

Une bonne compréhension des tâches professionnelles d'un stagiaire à distance ne garantie absolument pas sa gestion des problèmes d'environnement de travail.

Ne pas s'attendre à ce que le stagiaire sache comment bien travailler dans un environnement distant, surtout si c'est sa 1ère expérience professionnelle. Divers guides sur le travail à distance peuvent être utile, mais attention à ceux ayant pour prérequis/à priori une précédente expérience professionnelle en présentielle.


### Sécurité psychologique

Sur site la proximité crée généralement de la sécurité psychologique permettant au stagiaire de poser des questions au fur et à mesure qu'elles se présentent, sans crainte de s'imposer.

- prise de contact à l'initiative du mentor avec le stagiaire à un rythme régulier (une fois par heure)
- rappel quotidien que les stagiaires sont libres de se questionner le mentor aussi souvent que nécessaire, comme si il était dans la même pièce
- avoir un salon vidéo toujours ouvert où le stagiaire peut voir au moins une autre personne travailler (2ème écran)


### En cas d'embauche

Rechercher les compétences suivantes :

- une capacité d'auto-apprentissage
- l'autonomie
- appétence pour la documentation
- capacité à travailler de manière asynchrone

Si le stagiaire est sans expérience professionnelle, les questions suivante permettent de se faire une idée :

> Avez-vous déjà suivi un cours ou une formation en ligne ? Si oui, décrivez votre processus pour gérer votre propre temps, respecter les délais, demander de l'aide et résoudre les problèmes :

> Êtes-vous un membre actif d'une communauté en ligne, ou avez-vous une expérience de la gestion logistique à distance pour des événements/réunions sur site ?

> Avez-vous déjà travaillé en indépendant pour des clients à distance et seriez-vous prêt à fournir des références sur demande ?

> Avez-vous réfléchi à l'endroit où vous travailleriez à distance si on vous offrait ce stage ?

Ne pas s'attendre à une réponse précise à cette question, car même les cadres supérieurs doivent expérimenter les espaces de travail, mais il est utile de comprendre si le candidat a tenu compte de cet élément :

> Parlez-moi d'une époque où vous avez fait avancer un projet de groupe dans un lieu physique différent de celui des autres membres

_Les étudiants universitaires auront de fortes chances d'avoir travaillé à distance. Cherchez des détails sur la façon dont ils l'ont apprécié, les défis qu'ils ont relevés et comment ils y ont fait face._

### Durée du stage

Rallonger du temps de stage permet de créer un moment d'acclimatation au travail à distance (3 mois sur site ~ 4 mois à distance)


### Début / fin de stage sur site

Sans être obligatoire, envisager une semaine de stage sur site. Surtout pour une embauche de plusieurs stagiaires au sein d'une même équipe.

Une dernière semaine sur site peut être utile pour réaliser un bilan et la suite.


### Gérer les attentes

Calibrer les attentes et ce que l'on évalue est important, car les stagiaires à distance apprennent essentiellement deux choses :
- le travail lui-même
- la façon de bien travailler à distance (sans bureau)

Cela peut s'avérer particulièrement difficile quand l'éloignement est important : ces entreprises ont tendance à être très efficaces. Quand ce sont les résultats, et non les heures, qui sont mesurés, il y a une tendance naturelle à s'attendre à la rapidité. Le stagiaire à distance apprend deux grandes leçons en même temps et les progrès peuvent sembler plus lents que ceux auxquels on s'attend.


### La sélectionner des projets proposés

Certains projets et certaines tâches sont plus facilement réalisés par le personnel _junior_. En outre, certaines tâches sont plus facilement accomplies de manière asynchrone.

Veiller à ne pas se décharger des «_vampires énergétiques_» sur le stagiaire à distance, mais plutôt choisir des projets complets qu'il peut commencer et terminer en quelques mois.

Il est également important de sélectionner des projets importants ou impliquant un grand groupe de collaborateurs : cela permet de s'assurer que de nombreuses personnes s'investiront dans la réussite du stagiaire, et d'élargir le champ des mentors disponibles qui connaissent bien le projet et peuvent intervenir/aider.

Dans la mesure du possible, évitez les travaux où le temps est compté. Les grands projets de stage sont généralement considérés comme "agréables à réaliser" par le personnel à plein temps. Il s'agit de projets dont tout le monde reconnaît qu'ils seraient bénéfiques pour l'organisation, mais qui n'atteignent pas le niveau de priorité nécessaire pour exiger l'attention de la hiérarchie.


### Publier votre stratégie et votre embarquement

C'est offrir un contexte abondant à la question :

> Comment se passe le travail dans votre entreprise ?

Cela témoigne du respect des demandeurs d'emploi et permet que les candidats soient en accord avec vos valeurs.

Il est difficile d'embaucher des stagiaires à distance. Une organisation ne devrait pas rendre les choses plus difficiles en dissimulant sa vision et ses valeurs jusqu'à l'embauche d'un stagiaire. Compte tenu de la durée remarquablement courte de l'expérience de stage, il est essentiel d'agir de manière transparente afin de créer un alignement aussi large que possible avant le départ.


### Mais alors, pourquoi proposer des stages à distance ?

#### Avantage concurrentiel à l'embauche

Nous pensons [_GitLab inc._, NdT] que le travail à distance est l'avenir du travail, et la possibilité de travailler là où l'on est le plus épanoui deviendra bientôt la norme. Les entreprises qui sont construites pour soutenir le travail à distance (par exemple les entreprises qui créent des produits numériques), mais qui refusent d'offrir une telle flexibilité, seront peu sollicitées par les meilleurs talents. Cela inclut les stagiaires les plus prometteurs.

Autrement dit, offrir des stages à distance aujourd'hui constitue un avantage concurrentiel pour attirer des stagiaires ambitieux et compétents, mais cela va bientôt devenir une exigence pratique.


#### Diversité

En engageant des stagiaires à distance, les organisations sont en mesure d'élargir leur champ d'action habituel et de recruter des talents dans des régions mal desservies du globe. La diversité culturelle et géographique est importante pour la réussite et le dynamisme à long terme d'une entreprise.


#### Un regard neuf

Les stagiaires sont capables d'offrir des perspectives intéressantes. Étant donné que beaucoup ne travaillent que quelques mois, ils sont capables d'évaluer une organisation avec un regard neuf, et ils ont peu à perdre en offrant un retour d'information transparent en cours de route.

Les stagiaires qui sont à l'université ou récemment diplômés peuvent aussi apporter de nouveaux outils et de nouvelles méthodologies.


#### Remarques relatives aux stagiaires

Les stagiaires qui envisagent de postuler pour un stage à distance plutôt que pour un stage sur site doivent suivre les conseils ci-dessus afin de mieux comprendre le point de vue de l'employeur.

Il est important de reconnaître que les attentes seront probablement plus élevées dans un stage à distance, alors que les ressources peuvent être moindres si l'entreprise ne dispose pas d'une infrastructure de stage mature.

Il est important de poser des questions pendant la phase d'entretien afin de comprendre l'environnement de travail. Vous devez vous assurer d'avoir un mentor à distance ou un référent d'accueil qui sera disponible pour répondre aux questions relatives au travail à distance.

Le travail à distance vous oblige à faire des choses que vous devriez faire de toute façon, mais plus rapidement et plus intentionnellement. C'est un excellent contexte pour les stagiaires, qui peuvent non seulement acquérir une expérience professionnelle, mais aussi travailler dans un environnement où ils sont maître de leur espace, de leur temps et de leur approche.

---


Bonnes pratiques pour créer un stage à distance réussi
------------------------------------------------------


### Une communication claire

S'assurer que :
- toutes leurs parties prenantes soient bien informées de l'avenir de leurs projet
- les étudiants savent à quoi s'attendre et quand

[Molly DeCastro](https://www.linkedin.com/in/mollydecastro) a commencé à constater un mouvement vers les stages à distance de la part de plusieurs entreprises technologiques qui recrutent, à [HBS](https://www.hbs.edu/) ou en dehors.

Une action claire associée à une communication directe avec les stagiaires est bénéfique même si les détails du stage ne sont pas encore finalisés. Dans un environnement économique incertain, les stagiaires sont préoccupés par l'annulation des stages. Par conséquent, le fait d'agir rapidement pour faire avancer un stage à distance contribue grandement à établir la confiance. En outre, en prenant une décision au plus vite et en la faisant connaître au(x) nouveau(x) stagiaire(s), votre entreprise aura plus de temps pour élaborer une expérience de stage aussi efficace que si elle était sur site.


### Conception du stage à distance

Pour concevoir un stage à distance efficace, regardez ce qui a fait le succès des stages sur site dans votre entreprise et dans d'autres. [Kristen Fitzpatrick](https://www.linkedin.com/in/fitzpatrickkristen) (HBS 2003), directrice générale de _Career & Professional Development_, a noté les points communs entre les grandes expériences de stage :

> Les stages réussis profitent à la fois aux organisations et aux étudiants en donnant la possibilité à une personne ayant un regard neuf de jeter un regard sur la façon dont les choses sont faites, et en permettant aux étudiants de mettre à profit leur expérience antérieure dans une nouvelle situation. Les étudiants ayant vécu des expériences particulièrement positives font souvent remarquer qu'ils se sont sentis intégrés dans le travail de l'équipe et que les gens de l'organisation se sont souciés de ce sur quoi ils travaillaient.

Pour reproduire cette intégration, réfléchissez à la manière dont vous pouvez créer une structure en ligne de soutien, de mentorat et de formation continue. Les étapes que _K. Fitzpatrick_ suggère souvent :

- des rencontres régulières en face à face avec entre stagiaire et mentor
- un point au milieu [du stage NdT]
- une présentation à la fin [du stage NdT] avec les dirigeants en visioconférence
- un travail quotidien par chat, outils de gestion de projet et e-mail

Non seulement ces étapes annoncées donneront à votre stagiaire des jalons à atteindre, mais ils lui permettront également de faire connaissance avec les membres de l'équipe et les dirigeants à distance.

[Darren Murph](https://www.linkedin.com/in/darrenmurph), responsable du travail à distance chez _GitLab_, souligne également l'importance de choisir les projets appropriés pour les stagiaires à distance :

> Il est important de sélectionner des projets qui sont importants ou notables pour un grand groupe. Cela permet de s'assurer que de nombreuses personnes de l'organisation sont investies dans la réussite du stagiaire, et cela élargit le champ des mentors disponibles qui connaissent le projet et peuvent intervenir et aider à travers les fuseaux horaires.

Vous pouvez également envisager un stage en rotation qui donnera aux étudiants un aperçu des différents aspects de votre entreprise tout en n'étant pas sur place avec votre équipe. [Keirsten Sires](https://www.linkedin.com/in/keirsten-sires-83928853), consultant en matière de stages à distance et fondateur et PDG de _LRT Sports_, utilise ce modèle depuis quatre ans dans le cadre du stage de rotation à distance de 10 semaines de l'entreprise. Les étudiants passent par cinq rotations avec des projets assignés chaque semaine et revus par des mentors. La dernière rotation permet aux stagiaires de choisir le domaine d'activité qui les intéresse le plus pour réaliser un projet final. Elle a également souligné la nécessité d'être flexible au sein de votre structure :

> Notre stage est très différent de ce qu'il était lorsque nous avons commencé. Vous devez être dynamique dans ce processus et obtenir un retour d'information. Ensuite, si quelque chose ne fonctionne pas, n'essayez pas de le forcer.


### Embarquer à distance

Une expérience d'embarquement à distance pour vos stagiaires ne sera pas remplie de poignées de main autour du bureau et d'un déjeuner d'équipe. Cependant, vous pouvez les accueillir efficacement et faire en sorte qu'ils se sentent intégrés dès le premier jour grâce à des processus efficaces et un soutien continu.

Par exemple, tous les employés de _GitLab_sont à distance et ont donc mis au point un processus d'intégration qui comprend un compagnon d'intégration, des listes de contrôle des tâches spécifiques et un manuel complet de l'entreprise. Le manuel en ligne de 5 000 pages est constamment mis à jour à mesure que les employés trouvent des lacunes, de nouveaux outils ou des moyens plus efficaces d'accomplir leur travail. Il n'est pas destiné à être lu du début à la fin, mais à servir de guide de référence pour les questions. Pour son programme de stage en particulier, _D. Murph_ recommande un embarquement avec des points à un rythme régulier, jusqu'à plusieurs fois par jour pendant la période de démarrage, afin de reproduire une expérience sur site. Vous pouvez également installer une salle de visioconférence "toujours allumée" pour encourager les questions et l'interaction.

À _LRT Sports_, _K. Sires_ a mis en place un processus d'embarquement qui met l'accent sur les points suivants :

- découvrir l'entreprise grâce à l'accès aux ressources et aux médias sociaux avant l'arrivée du stagiaire
- après deux semaines, chaque stagiaire présente l'entreprise à son responsable
- l'éducation continue avec des leçons vidéo et des articles fournis


### Gestion d'une équipe virtuelle

La technologie a rendu le travail à distance plus facile que jamais et il existe de nombreux outils qui aident les équipes à collaborer. Cependant, il existe des équipes à distance depuis des décennies dans divers secteurs et la réussite de ces entreprises repose en grande partie sur une gestion efficace, fondée sur le soutien et la confiance. Ces pratiques s'appliquent également à la gestion de stages à distances.

[Claire Lew](https://www.linkedin.com/in/clairelew), PDG de _Know Your Team_, a dirigé son entreprise à distance pendant 6 ans et fournit des outils et des ressources aux autres entreprises pour qu'elles fassent de même. Les conseils qu'elle donne en matière de gestion d'équipes virtuelles sont axés sur la définition d'attentes claires quant à la réussite, ainsi que sur des systèmes de suivi des progrès. En préparant le terrain très tôt avec tous les employés, et en particulier les nouveaux stagiaires, vous pouvez éviter la micro-gestion des projets et créer plutôt une culture de confiance et donner à l'équipe le temps et l'espace nécessaires pour effectuer un travail percutant.

Il est également important de définir des attentes claires en matière de communication. Si votre équipe dispose d'un mode de communication par défaut, assurez-vous que les stagiaires sont bien informés des canaux à utiliser et du moment où ils le font. Dans quel délai les stagiaires sont-ils censés répondre aux messages ? Quelle est la place du chat et qu'est-ce qui convient le mieux à une réunion par courriel ou visioconférence ? Lors d'un récent webinaire _Know Your Team_, _C. Lew_ a donné des exemples d'entreprises qui font un excellent travail en fixant des attentes en matière de communication, notamment _Automatic_, _Buffer_ et _Basecamp_.

_C. Lew_ , ainsi que _D. Murph_ de _GitLab_, recommandent également de mettre en œuvre une pratique de communication asynchrone au lieu de se reposer sur la visioconférence. Lancer des séances de brainstorming par écrit ou poster des questions sur des documents partagés permet aux membres de l'équipe, y compris les stagiaires, de réfléchir et d'être ensuite moins réactifs et de prendre des décisions plus réfléchies. En intégrant des stagiaires à distance dans votre équipe, l'écriture asynchrone permet également de collaborer au-delà des fuseaux horaires et de fournir des documents écrits sur la façon dont les stagiaires ont apporté des contributions percutantes.


### Trouvez ce qui vous convient

N'oubliez pas que nombre des entreprises présentées ici gèrent des programmes de stages à distance depuis des années et qu'elles ont toutes eu le temps d'élaborer leurs stratégies et d'apporter des changements en cours de route. En cette période de transition entre aujourd'hui et l'été 2020, nous vous encourageons à réfléchir au temps et aux ressources dont dispose votre entreprise et à choisir parmi ces meilleures pratiques celles qui vous permettront de tirer profit d'un stage d'été à distance.

En outre, à mesure que vous progresserez dans la gestion de vos stagiaires virtuels cet été, vous pourrez également vous référer aux meilleures pratiques de gestion à distance et au leadership éclairé de la faculté de la _Harvard Business School_.

---


Notes de fin
------------

Vous avez lu jusqu'ici ? Merci !

J'ai commencé ce billet avant, mais l'ai terminé après le démarrage du stage. Dans une TPE, les possibilités sont limités mais la proximité plus facile a construire, à fortiori quand le·a collaborateur·ice à déjà une dizaine d'année d'expérience professionelle, ce qui était notre cas.

Concrètement nous avons pu mettre en places quelques actions pérène :

- un point visio quotidien, informel à heure variable (occasionnellement deux, des fois pas)
- deux jours sur site pour mon collègue _sans moi_
- une journée sur site _avec moi_
- un chat ouvert, où _tous_ les collègues ont un accès
- un outil de gestion de projet de développement (instance privée de GitLab) avec :
    - une organisation structurée des projets de code ([version publique](https://gitlab.com/forga))
    - un outil de suivi d'activité/tutorat ([version publique](https://gitlab.com/forga/process/fr/embarquement/-/blob/production/README.md))
    - une source de documentation unique ([version publique](https://gitlab.com/forga/process/fr/manuel/-/blob/production/README.md))
- pas d'e-mail. En tout cas pas entre nous deux.
- pas de téléphone, SMS, messagerie.

L'expérience n'est pas terminée, mais je peu dors et déjà recommander de tenter l'expérience.
