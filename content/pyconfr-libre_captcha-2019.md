Title: Un Captcha libre, c'est possible !
Date: 2019-11-02 17:00
Summary: Pour lutter contre les abus de Google un prototype fonctionnel libre à vu le jour
Category: Bloc-notes
Tags: pyconfr, talk, bordeaux, captcha, logiciel libre, python
Status: published

### [Un Captcha libre, c'est possible !][1]

Par [Mindiell][2] − Salle Thomas Edison − Samedi à 16 h 30

![logo PyConFr Bordeaux 2019][pyconimg]
Le Captcha (ou Completely Automatized Public Turing test to tell Computers and Humans Apart) est un moyen a priori simple de déterminer si l'utilisateur situé derrière un site web est un humain ou un robot. Cet outil est aujourd'hui souvent synonyme de ReCaptcha, technologie rachetée par Google en 2009. Malheureusement, cet outil est surtout utilisé à des fins de surveillance et d'espionnage, voire pour améliorer des IA utilisées par l'armée américaine.

C'est en souhaitant lutter contre les abus de Google via cette technologie que j'ai travaillé sur un prototype fonctionnel qui pourrait facilement remplacer l'actuel. Pas forcément plus compliqué, pas forcément plus fiable, ce test est, par contre, beaucoup plus libre, son code source est ouvert et chacun peut l'installer chez soi (ou dans son entreprise).

On parlera donc de ce qu'est un Captcha, de ce que ça demande comme travail de recherche pour générer des tests de manière automatisée, de gérer des tests pour les déficients visuels (accessibilité quoi), de l'adapter pour remplacer un ReCaptcha le plus facilement possible, bref de comment faire un Captcha de A à Z !

---

Notes personnelles:

[LibreCAPTCHA][4]

- 1997 CAPTCHA
- 2007 ReCAPTCHA
- 2009 Aquisition Google
- 2011 Scan archive NYT & Google books
- 2015 Aide à la traduction
- 20??  usage image gStreetView
- 2018 ReCAPTCHA invisible

### Usages collatéraux

- optimisation OCR
- [projet Maven][5]
- les robots spammeeurs utilisent finalement les tests audio plus faciles

### Génération automatique

- 14 millions de CAPTCHA / semaine sur eBay
- non répétabilité

### Acces aux déficients

- visuel : 8%
- auditif : 5%
- cognitif : 16%

### Stats

- tests visuels : 71% de bonne réponse
- tests audios : 31% de bonne réponse

### [LibreCAPTCHA][4]

Définir challenge visuel :

- Image de WikiCommons
- proposition de 9 vignettes issues d'autres image, sauf une

Définir challenge audio :

- séquence de son sur lesquelles on ajoute du morse
- bibliothèque CCMixer

### Quitter ReCAPTCHA

Remplacement des 2 URLs : JS & clef

pré-génération indispensable par lot /!\ au appel wiki & CCMixer

Pas de collecte de meta-donnée

[pyconimg]: {static}/img/250-pycon-fr19.png

[1]: https://www.pycon.fr/2019/fr/talks/conference.html#un%20captcha%20libre%2C%20c%27est%20possible%E2%80%AF%21
[2]: https://mindiell.net/
[4]: https://framagit.org/Mindiell/captcha
[5]: https://en.wikipedia.org/wiki/Project_Maven
