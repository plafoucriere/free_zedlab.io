Title: Générateur de site statique, et pourquoi pas ?
Date: 2019-04-07 13:00
Modified: 2019-10-24
Category: Conférences
Status: published
Summary: Les générateurs de site en général et Pelican en particulier
Tags: jdll, talk, lyon, pelican, gitlab, gitlab-pages, statique, live-coding, web, ci, cd, git, python,

![Logo des JDLL 2019][jdlllogo]

Les 6 & 7 avril 2019 se sont déroulées les [Journées du Logiciel Libre][jdll] à Lyon.


J'y ai présenté les _générateurs de site statique_ en général et [Pelican][pelican] en particulier.

Le format d'une heure aura permis une rapide présentation suivie d'une séance d'installation en direct [d'un site généré par _Pelican_][site] avec hébergement via [gitlab-pages][glpages].

Les sources & supports sont dispo sur le dépôt gitlab : [free_zed/jdll19][repo].

Une captation vidéo à été mise en place et sera peut-être dispo dans les prochains jours.

---
[MàJ 2019-10-24]

La vidéo [est disponible][video] !

[repo]: https://gitlab.com/free_zed/jdll19
[jdll]: https://jdll.org
[jdlllogo]: {attach}/img/jdll-logo.png
[pelican]: http://getpelican.com/
[glpages]: https://docs.gitlab.com/ce/user/project/pages/
[site]: https://staticbird.gitlab.io/
[video]: https://www.videos-libr.es/videos/watch/36ff9d04-83ae-4ad3-92a6-d6310658d4eb
