Title: Postgresql : haute dispo et réplication
Date: 2020-04-16 18:01
Category: Bloc-notes
Status: published
Summary: Postgresql, des notions de haute disponibilité et de réplication
Tags: talk, lyon, postgresql, devops,

Par [Raphaël Ruelle][1], organisé par [Café Devops][cafedevops] (via [Meetup][meetup]). Archive sur [Café Devops][support] (_à venir_).

Il nous aura fallu quelques semaines d'adaptation, mais désormais notre soif de partage de bonnes pratiques ne peut rester sans réponses.

La prochaine présentation portera sur [`Postgresql`][3] et le management de la réplication et du Failover d'un cluster avec [`Repmgr`][2].

La session débutera par une présentation des notions de réplication, de journaux, failover, split brain, puis viendra une démo.

---

Notes personnelles
==================

- réplication synchrone : tous les serveurs doivent valider les transactions
- réplication asynchrone : tous les serveurs peuvent ne pas valider les transactions : une tempo est en place
- réplication logiques vs physiques (?)

- Cas d'usage de la présentation
    - Choix d'utiliser WAL (Write Ahead Logging) :
        - journaux intérmédiaire moins gourmand en ressource à utiliser
        - 12 (?) fichiers sur lesquels ont boucle
    - accès en lecture sur standby

Pour un pool de server :

- [`Repmgr`][2]
    - admin et configuration
    - priorisation par poids pour choix du serveur primaire
    - promotion/inversion (admin/standby)
- `Repmgrd` : Demon de supervision
    - réalise le failover si primaire HS

Cas de défauts :

- split-brain : data center isoléé
- Perte de tous les serveur d'un lieu (DC)


[1]: https://www.linkedin.com/in/rapha%C3%ABl-ruelle-4128745a
[2]: https://repmgr.org/
[3]: https://www.postgresql.org/
[cafedevops]: https://cafedevops.org/
[meetup]: https://www.meetup.com/fr-FR/cafe-devops-lyon/events/269957119/
[support]: https://cafedevops.org/posts/
[pdf]: https://cafedevops.org/pdf/
