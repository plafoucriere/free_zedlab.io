Title: Mise à jour d'un réseau local Debian avec Ansible
Date: 2020-04-19 00:43
Category: Bloc-notes
Status: published
Summary: Mise à jour d'un parc Debian hétérogène (packages &_config), avec packages hors dépôt.
Tags: ansible, admin, debian, mate, devops, shell, git, dry

### Contexte

Mise à jour d'un parc Debian hétérogène, pour l'exercice, le réseau famillial fera l'affaire :

- 5 machines physiques : 1 serveur +  4 postes (Stretch/Buster/Bullseye)
- 2 machines virtuelles, (Jessie/Buster)

### Besoin

- machines à jour
- instalations homogènes : packages et configuration
- gérer des packages `*.deb` disponible hors dépôt `apt`

### _Talk is cheap. Show me the code._

- [Installer Ansible][install]
- récupérer [le _playbook_][repo] : `git clone https://gitlab.com/free_zed/myasb.git`
- se mettre dans la bonne branche : `cd myasb; git checkout lan`
- adapter [l'inventaire][host] avec les adresses de vos machines : `$EDITOR hosts`
- jouer le _playbook_ : `ansible-playbook -i hosts site.yml`
- …et volà!


### Remarques

`git` n'utilise pas la configuration globale par défaut (`/etc/gitconfig`), je m'en suis servi pour déployer mes alias _partout_ sans déployer la config `git` de ma machine de dev qui contient des éléments que je ne souhaite pas diffuser (tokens, mails, usernames, tools, etc).

### Idées

- centraliser le build & déploiement d'un projet Java sur Github ([Mindustry][mindustry]).
- comparer le numéro de version Debian plutôt que le nom (`str()` to `int()` des _facts Ansible_)
- faire des taches/roles/whatever qui ne se font qu'une fois avant (ou après) de jouer le playbook
    - récupérer la version stable Debian
    - récupérer numéro de dernière version d'un `*.deb` dispo hors dépot
    - envoyer _un seul_ mail de récap
- utiliser les variables d'environnement (ou autre) pour les données privées


[repo]:         https://gitlab.com/free_zed/myasb/-/tree/lan
[mindustry]:    https://github.com/Anuken/Mindustry/
[install]:      https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
[host]:         https://gitlab.com/free_zed/myasb/-/blob/lan/hosts
