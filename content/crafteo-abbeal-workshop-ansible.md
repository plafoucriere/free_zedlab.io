Title: e-Workshop : introduction à Ansible
Date: 2020-07-29 19:00
Category: Bloc-notes
Status: published
Summary: L'objectif de cet atelier est de découvrir Ansible par la pratique.
Tags: workshop, lyon, ansible, devops
Translation: false
Lang: fr

Par [Pierre Beucher][pierre-beucher] et organisé par [Abbeal][abbeal] & [Crafteo][crafteo] (via [Meetup][meetup]).

Atelier pratique d'introduction à Ansible
=========================================

L'objectif du meetup est de découvrir [Ansible][ansible] sous forme d'un atelier pratique. A la fin de la séance, nous aurons introduit les concepts de base d'Ansible:

- Qu'est-ce qu'Ansible et l'Infrastructure as Code?
- Écrire et utiliser un Playbook Ansible
- Inventaires, Tâches, Modules, Variables et Templates
- Découper un Playbook en Rôles
- Ansible Galaxy
- Overview des features avancées d'Ansible

Assurez-vous d'avoir un système Linux (préférablement Ubuntu 18 ou 20) directement sur votre machine ou avec une VM

Pour pleinement profiter de l'atelier il est conseillé d'avoir les connaissances suivantes:
- Connaissance de base en système Linux
- Connaissance de base en réseau: savoir ce qu'est une adresse IP, un port, un nom d’hôte, etc.
- Connaissance de base en système de fichier Linux (connaître une arborescence de fichier, gérer les droits, etc.)
- Savoir utiliser un shell Linux (commandes de base type apt-get install)
- Savoir utiliser un éditeur en console (type vi ou nano) ou avec interface graphique

15 places disponibles !

Nous vous attendons nombreux avec le sourire & l'envie d'échanger !

Workshop
--------


2 machines par élèves (construite avec [`PierreBeucher/cloud-sandbox-manager`][cloud-sandbox-manager]):

- `ubuntu@user1.training.crafteo.io`
- `ubuntu@user2.training.crafteo.io`

### Base de travail

- [`PierreBeucher/workshop-ansible-playbook`][workshop-ansible-playbook]

### Exercices

- partie 1: [hello world][1]
- partie 2: [les tâches][2]
    * docs : [filtres][filters]
- partie 3: [les variables][3]
    * docs : [variable precedence][precedence]
- partie 4: [les roles][4]
- partie 5: [Ansible Galaxy][5]

### Dépôt de travail : 

- [`free_zed/ansible-crafteo`](https://gitlab.com/free_zed/ansible-crafteo)


Notes personnelles
------------------

r-----------------

r-----------------

r-----------------

Voir pour fusionner les exemples ici avec [`free_zed/myasb`](https://gitlab.com/free_zed/myasb) pour ajouter à [`forga/process`](https://gitlab.com/forga/process)


[1]: http://ansible.training.crafteo.io/hello
[2]: http://ansible.training.crafteo.io/tasks
[3]: http://ansible.training.crafteo.io/vars
[4]: http://ansible.training.crafteo.io/roles
[5]: http://ansible.training.crafteo.io/galaxy
[abbeal]: http://www.abbeal.com/
[ansible]: https://www.ansible.com/
[cloud-sandbox-manager]: https://github.com/PierreBeucher/cloud-sandbox-manager
[crafteo]: https://crafteo.io/
[filters]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html
[meetup]: https://www.meetup.com/fr-FR/meetup-group-IzAHXpzE/events/271984246/
[pierre-beucher]: https://www.linkedin.com/in/pierre-beucher/
[precedence]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable
[workshop-ansible-playbook]:https://github.com/PierreBeucher/workshop-ansible-playbook
