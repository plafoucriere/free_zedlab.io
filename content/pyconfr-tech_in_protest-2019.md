Title: The role of 21st century technology in protests
Date: 2019-11-03 14:30
Summary: Hong Kong shocked the world with massive protests, protesters are mainly young, educated and technology-aware people and are trying to use the new technology to outwit the government.
Category: Bloc-notes
Tags: pyconfr, talk, bordeaux, hong-kong, citoyen, logiciel libre, python
Status: published
Lang: en

### Plénière n°3 / Keynote #3: [The Role of 21st Century Technology in Protests][1]

Par [Cheuk Ting Ho][2] − Salle [Alfred Wegener][awegener] − Dimanche à 14 h 00

![logo PyConFr Bordeaux 2019][pyconimg]
Since June 2019, Hong Kong shocked the world with massive protests. As protesters are mainly young, educated and technology-aware people, they are trying to use the new technology to outwit the government which also use AI and technology to control its people. Who got the upper hand?

Have you ever think about using Tinder to look for a ‘date’ in the protest? Or using Uber to call for a free ride to drive you to safety?

In the first part of the talk, we will cover the background of the protest, why it happened and how it is started. We will briefly talk about the politic environment of Hong Kong and what causes the protest 1: the fear of losing a “high degree of autonomy” In the second part, which is the main part of the talk, we will cover how technology/ apps like Telegram, Tinder, Pokemon Go, Twitch, AirDrop, Bridgefy, Uber, etc were used in by the protesters and what role they play in the protest. People use Telegram’s encryption to communicate without worrying leaking of identity; they use Tinder to spread the news about where and when to protest; They use Pokemon Go to gather people 4; AirDrop to spread the message to the Chinese tourists; Bridgefy to communicate where internet is not available at crowded areas 3; Uber to let the protested know where there are volunteer drivers to take you to safety.

In the third part, we will have a look at what technology the Chinese government may use to tighten its grip over Hong Kong. One of the front lines of the protest in the online activism 5, the Chinese government has it’s own ‘50 cent army’ which is also known as ‘China bots’ who were paid to leave pro-CCP comments on social media. Which eventually cause Twitter and Facebook to shout down lots of accounts. Also, there are attacks cyberattacks, suspected to be related to the protest, towards Telegram and an online forum LiHKG which the protesters used to communicate. It is also suspected that the government installed ‘smart’ lamppost which use facial detection to monitor the citizen of Hong Kong after protested found parts made by a Shanghai company which also made the surveillance cameras in mainland China 6.

This talk is for audients who are interested to know how our lives are changed by technology and how they could influence our society. By the end of the talk, audients will be more aware of what consequence new technology may bring us and be more thoughtful about the ethics behind the implementation of technology.

---

Personal notes :

créatrice de [pick & mix][3]

- 9/6 1M HK protesters
- 16/6 2M HK protesters
- 24/6 crowdfounding for ad in press
- 1/7 100 000 HK annual democracy march

Gov reaction :

- 11/8 eye injury
- 1/10 1st protester shot

    - 2022 arrest since
    - 4138 tear gaz canisters used
    - 1733 rubbuer bullet shot

Apps related to protests:

- tinder : leave message on status
- LiHKG, but moved on reddit : share useful links
- telegram : encription chat & **hudge** groups
- twitch : recording _real_ video
- [bridgefy][4] : bluetooth network with rebound
- uber : volunteer drivers help protesters but police arrest now driving
- pokemon go : used for an excuse for illegal assembly
- [whatsgap][5] : suggestion for protester supporter restauration & Week end activities
- _unknow_ : an app to pay taxe dollar by dollar (& cost 2$ for eac dollar payed to governement)

Solution to protect from technology :

- technology footprint is identity
- using single ticket instead  _packages_
- …

China use:

- public image face recognition
- social credit system
    - not perfect : women picture on a bus catched as a non footwalk usage
- kids at school
- lampposts are suspected to be used for filming
- reeducation camp

3 days ago chinese announce to boost effort for _security_

[1]: https://www.pycon.fr/2019/fr/talks/keynote.html#pl%C3%A9ni%C3%A8re%20n%C2%B03%20%2F%20keynote%20%233%3A%20the%20role%20of%2021st%20century%20technology%20in%20protests
[2]: https://dev.to/cheukting_ho
[3]: https://picknmix.readthedocs.io/en/latest/
[4]: https://en.wikipedia.org/wiki/Bridgefy
[5]: https://www.dimsumdaily.hk/whatsgap-a-new-app-developed-by-local-netizen-where-users-can-choose-to-go-to-restaurants-with-similar-political-stand/
[awegener]: https://en.wikipedia.org/wiki/Alfred_Wegener
[pyconimg]: {static}/img/250-pycon-fr19.png
