Title: Contact
Lang: en
Slug: contact
Status: published
Summary: Contact me!

Hopla !
-------

Welcome to this page, my name is _Frédéric Zind_.

Professional in **vehicle maintenance** and **software development**, I share here :

- some [notes][notes]
- things [I've done][real]
- as well as my [talkative performances][conf]

To contact me :

- mail : [pro@zind.fr](mailto:pro@zind.fr)
- phone : [+336 59 44 28 36](tel:+33659442836)
- [my _resume_][cvdown]

I'm on the networks mentioned in the footer and member of the [_Gitlab Hero_][glh] community since November 2019.

---

[conf]: /conferences/
[cvdown]: https://freezed.me/index.php/s/VM3F2r0wNXsOJDj/download#
[glh]: https://about.gitlab.com/community/heroes/members/
[notes]: /bloc-notes/
[real]: /realisations/
