Title: Contact
Lang: fr
Slug: contact
Status: published
Summary: Contactez moi !

Hopla !
-------

Bienvenue sur cette page, je m'appelle _Frédéric Zind_.

Professionnel en **maintenance des véhicules** et en **développement logiciel**, je partage ici :

- des [notes][notes]
- ce que [j'ai pu faire][real]
- ainsi que mes [prestations bavardes][conf]

Pour me contacter :

- mail : [pro@zind.fr](mailto:pro@zind.fr)
- tel : [+336 59 44 28 36](tel:+33659442836)
- [mon _CV_][cvdown]

Je suis présent sur les réseaux dont les icônes sont en pied de page et membre de la communaté [_Gitlab Hero_][glh] depuis novembre 2019.

---

[conf]: /conferences/
[cvdown]: https://freezed.me/index.php/s/VM3F2r0wNXsOJDj/download#
[glh]: https://about.gitlab.com/community/heroes/members/
[notes]: /bloc-notes/
[real]: /realisations/
